package com.zhm.user.mapper;

import com.zhm.user.domain.AppUser;
import com.zhm.user.dto.AlbumResponseDTO;
import com.zhm.user.dto.UserCreateRequestDTO;
import com.zhm.user.dto.UserResponseDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    @Autowired
    protected BCryptPasswordEncoder bCrypt;

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "encryptedPassword", ignore = true)
    //@Mapping(target = "encryptedPassword", expression = "java(bCrypt.encode(dto.getPassword()))")
    @Mapping(target = "lastName", source = "familyName")
    @Mapping(target = "firstName", source = "givenName")
    public abstract AppUser userCreateRequestDTOToUser(UserCreateRequestDTO dto);

    @AfterMapping
    public void userCreateRequestDTOToUser(UserCreateRequestDTO dto, @MappingTarget AppUser appUser) {
        appUser.setEncryptedPassword(bCrypt.encode(dto.getPassword()));
    }

    @Mapping(target = "givenName", source = "firstName")
    @Mapping(target = "familyName", source = "lastName")
    public abstract UserResponseDTO userToUserResponseDTO(AppUser appUser);

    public String UUIDToString(UUID uuid) {
        return uuid.toString();
    }


    @Mapping(target = "id", source = "user.id")
    @Mapping(target = "givenName", source = "user.firstName")
    @Mapping(target = "familyName", source = "user.lastName")
    @Mapping(target = "email", source = "user.email")
    @Mapping(target = "albums", source = "albums")
    public abstract UserResponseDTO UserWithAlbumsToUserResponseDTO(AppUser user, List<AlbumResponseDTO> albums);
}
