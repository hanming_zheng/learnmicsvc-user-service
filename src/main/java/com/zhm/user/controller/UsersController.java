package com.zhm.user.controller;

import com.zhm.user.domain.AppUser;
import com.zhm.user.dto.*;
import com.zhm.user.mapper.UserMapper;
import com.zhm.user.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/users")
public class UsersController {
    private Environment env;
    private UserMapper userMapper;
    private UserService userService;
    private AuthenticationManager authManager;
    private AlbumsFeignClient albumsFeignClient;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public UsersController(Environment env,
                           UserMapper userMapper,
                           UserService userService,
                           AuthenticationManager authManager,
                           AlbumsFeignClient albumsFeignClient) {
        this.env = env;
        this.userMapper = userMapper;
        this.userService = userService;
        this.authManager = authManager;
        this.albumsFeignClient = albumsFeignClient;
    }

    @Value("${eureka.instance.instance-id}")
    private String instanceId;

    @GetMapping("/status/ping")
    public String status() {
        return "Ping on port " + env.getProperty("local.server.port") + " instance-id: " + instanceId;
    }

    @GetMapping("/protected")
    public String protectedRoute() {
        return "Protect : Ping on port " + env.getProperty("local.server.port") + " instance-id: " + instanceId;
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDTO> login(@Valid @RequestBody LoginRequestDTO dto) throws Exception {
        try {
            // convert the dto into an authentication token
            Authentication credential = new UsernamePasswordAuthenticationToken(dto.getEmail(), dto.getPassword(), new ArrayList<>());
            // for spring to authenticate based on the above token, spring has to get user data from db through UserService
            Authentication authResult = authManager.authenticate(credential);
            // read user details from db to attach info to response
            AppUser appUser = userService.getUserByEmail(dto.getEmail());
            // generate jwt
            Map<String, Object> claims = new HashMap<>();
            claims.put("email", appUser.getEmail());
            claims.put("roles", Arrays.asList("normal-user", "admin-user"));
            String jwtToken = Jwts.builder()
                    .setClaims(claims) // each claim will be a custom key value pair in the jwt body
                    .setSubject(appUser.getId().toString())
                    .setIssuedAt(new Date(System.currentTimeMillis()))
                    .setExpiration(new Date(System.currentTimeMillis() + Long.parseLong(env.getProperty("token.expiration"))))
                    .signWith(SignatureAlgorithm.HS512, env.getProperty("token.secret"))
                    .compact();
            return ResponseEntity.ok(new LoginResponseDTO(appUser.getId().toString(), appUser.getEmail(), jwtToken));
        } catch (BadCredentialsException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping
    public ResponseEntity<UserResponseDTO> createUser(@Valid @RequestBody UserCreateRequestDTO dto) {
        AppUser userToSave = userMapper.userCreateRequestDTOToUser(dto); // encryption of password is in the mapper
        AppUser savedUser = userService.createUser(userToSave);
        UserResponseDTO userResponseDTO = userMapper.userToUserResponseDTO(savedUser);
        return new ResponseEntity<>(userResponseDTO, HttpStatus.CREATED);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<UserResponseDTO> getUserById(@PathVariable String userId) {
        AppUser retrievedUser = userService.getUserById(userId);
        logger.info("before calling albums service");
        List<AlbumResponseDTO> albums = albumsFeignClient.getAlbums(userId);
        logger.info("after calling albums service");
        UserResponseDTO responseDTO = userMapper.UserWithAlbumsToUserResponseDTO(retrievedUser, albums);

        return ResponseEntity.ok(responseDTO);
    }
}
