package com.zhm.user.controller;

import com.zhm.user.dto.AlbumResponseDTO;
import feign.FeignException;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@FeignClient(name = "album-service", fallbackFactory = AlbumsFeignFallbackFactory.class)
public interface AlbumsFeignClient {

    @GetMapping("/albums/by-user/{id}")
    List<AlbumResponseDTO> getAlbums(@PathVariable String id);
}

// fallback class for feign client above
// any time there is exception, will call methods in the class from the create method
@Component
class AlbumsFeignFallbackFactory implements FallbackFactory<AlbumsFeignClient> {
    @Override
    public AlbumsFeignClient create(Throwable cause) {
        return new AlbumFeignClientFallback(cause);
    }
}

// actual fallback class with Throwable passed in by factory
class AlbumFeignClientFallback implements AlbumsFeignClient {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private final Throwable cause;

    public AlbumFeignClientFallback(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public List<AlbumResponseDTO> getAlbums(String id) {
        // log exceptions for developers
        if (cause instanceof FeignException) {
            FeignException feignException = (FeignException) cause;
            if (feignException.status() == 404) {
                logger.error("404 on getAlbums with userId: " + id + " - " + feignException.getLocalizedMessage());
            }
        } else {
            logger.error("other error occured - " + cause.getLocalizedMessage());
        }
        // provide fallback for users
        return new ArrayList<>();
    }
}