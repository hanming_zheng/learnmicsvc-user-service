package com.zhm.user.dto;

import lombok.Data;

@Data
public class AlbumResponseDTO {
    private String albumId;
    private String name;
    private String description;
}
